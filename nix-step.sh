#!/bin/bash

nix-env -if https://github.com/Shopify/comma/archive/4a62ec17e20ce0e738a8e5126b4298a73903b468.tar.gz

# nix-env -i act
# nix-env -i diff-so-fancy
# nix-env -i bash
# nix-env -i coreutils
# nix-env -iA nixpkgs.awscli2
# nix-env -iA nixpkgs.iterm2
# nix-env -iA nixpkgs.elixir
# nix-env -iA nixpkgs.erlang
# nix-env -iA nixpkgs.php
# nix-env -iA nixpkgs.phpPackages.composer
# nix-env -iA nixpkgs.nodejs-13_x
# nix-env -iA nixpkgs.ruby_2_7
# nix-env -iA nixpkgs.go
# nix-env -iA nixpkgs.thefuck
